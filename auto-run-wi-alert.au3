#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile_x64=auto-run-wi-alert.exe
#AutoIt3Wrapper_UseX64=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Array.au3>

;  UI objects
Const $winToastTarget = "New Work Item"
Const $winToastAction = "[NAME:ToastAction]"
Const $winToastPickup = "[TEXT:Pickup]"

;  Timing
Const $winDelay = 100 ; milliseconds
Const $winTimeout = 30 ; seconds

;  Debugging
Local $resultCode = 0
Local $attemptCount = 0

;  Logging
Dim $resultCodes[5] = [ 0, 1, 3, 7, 15 ]

Dim $resultInfo[5] = [ "ERROR: Inactive()", _
   "ERROR: No Focus()", _
   "ERROR: No {ENTER}", _
   "WARN: No Close()", _
   "INFO: Success" ]

Local $resultIndex = -1

; ====

While True

   $result = WinWait($winToastTarget)
   Sleep($winDelay) ; milliseconds

   For $attemptCount = 1 To $winTimeout ; seconds

       Select

           Case $result == 15;  $winToastTarget closed successfully?
               ExitLoop

           Case $result == 7;  $winToastTarget still open?
               $result += WinClose( $winToastTarget ) * 8

           Case ( WinActive( $winToastTarget ) <> 0 ); $winToastTarget ready?

               $result = 1; active
               $result += 6 ; ControlFocus($winToastTarget, "", $winToastPickup) * 2
               Send( "{TAB}{TAB}{ENTER}" )

           Case Else ; $winToastTarget inactive?
               WinActivate($winToastTarget)

       EndSelect

   Next

   ; Logging�

   $resultIndex = _ArrayBinarySearch( $resultCodes, $resultCode )
   ; Log $resultInfo[$resultIndex] and variable values, if desired for the recorded result codes

Wend
